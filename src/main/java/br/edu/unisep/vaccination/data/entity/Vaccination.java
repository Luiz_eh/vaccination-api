package br.edu.unisep.vaccination.data.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Vaccination {

    private Integer id;

    private String patientName;

    private String patientPhone;

    private Integer vaccineTypeId;

    private LocalDate firstDosage;

    private LocalDate prevSecondDosage;

    private LocalDate actualSecondDosage;

    private Integer statusId;

}
