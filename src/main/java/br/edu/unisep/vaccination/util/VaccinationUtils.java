package br.edu.unisep.vaccination.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VaccinationUtils {

    public static final int ID_VACCINE_TYPE_A = 1;
    public static final int ID_VACCINE_TYPE_B = 2;
    public static final int ID_VACCINE_TYPE_C = 3;

    public static final String VACCINE_TYPE_A = "Vacina A";
    public static final String VACCINE_TYPE_B = "Vacina B";
    public static final String VACCINE_TYPE_C = "Vacina C";

    public static final int ID_STATUS_STARTED = 1;
    public static final int ID_STATUS_COMPLETE = 2;
    public static final int ID_STATUS_LATE = 3;

    public static final String STATUS_STARTED = "Vacinação iniciada";
    public static final String STATUS_COMPLETE = "Vacinação completada no prazo";
    public static final String STATUS_LATE = "Vacinação completada com atraso";

    public static final int INTERVAL_VACCINE_A = 21;
    public static final int INTERVAL_VACCINE_B = 40;
    public static final int INTERVAL_VACCINE_C = 90;

}
